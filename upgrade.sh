#! /bin/sh

# Copyright (c) 2023, Paranoid Android. All rights reserved.

# PA Changes:
# Strip binaries to optimize size (thanks Ju Hyung Park)

# Strip binaries:
find . -type f -exec file {} \; \
    | grep "x86" \
    | grep "not strip" \
    | grep -v "relocatable" \
        | tr ':' ' ' | awk '{print $1}' | while read file; do
            strip $file
done

find . -type f -exec file {} \; \
    | grep "ARM" \
    | grep "aarch64" \
    | grep "not strip" \
    | grep -v "relocatable" \
        | tr ':' ' ' | awk '{print $1}' | while read file; do aarch64-linux-android-strip $file
done

find . -type f -exec file {} \; \
    | grep "ARM" \
    | grep "32.bit" \
    | grep "not strip" \
    | grep -v "relocatable" \
        | tr ':' ' ' | awk '{print $1}' | while read file; do arm-linux-androideabi-strip $file
done

# Capture version number for automatic committing
VERSION=$(./compiler/bin/clang --version | grep 'version' | sed 's/clang version //g')

# Commit new version
git add compiler
git commit -m "Import stripped Snapdragon LLVM ARM Compiler $VERSION" -m "See compiler/RELEASE_NOTES and upgrade.sh for more information on what changed"
